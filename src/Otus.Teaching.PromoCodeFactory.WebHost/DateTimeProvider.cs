﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public interface IDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }

    public class CurrentDateTimeProvider : IDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}