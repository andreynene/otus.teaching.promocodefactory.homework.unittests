﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnersBuilder 
    {
        public static Partner CreateFull()
        {
            var partner = Create();
            var limit = CreatePromoCodeLimit();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                limit
            };
            return partner;
        }
        
        public static Partner Create()
        {
            return new Partner
            {
                Id = Guid.NewGuid(),
                Name = Guid.NewGuid().ToString("D"),
                NumberIssuedPromoCodes = 5,
                IsActive = true,
                PartnerLimits = null
            };
        }

        public static PartnerPromoCodeLimit CreatePromoCodeLimit()
        {
            return new PartnerPromoCodeLimit
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now.AddDays(1),
                Limit = 10
            };

        }
        
        public static Partner SetEndOfLimit(this Partner partner)
        {
            var limit = 10;
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            foreach (var param in partner.PartnerLimits)
                param.Limit = limit;
            partner.NumberIssuedPromoCodes = limit;
            return partner;
        }
    }
}